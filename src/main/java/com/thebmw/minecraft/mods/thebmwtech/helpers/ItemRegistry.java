package com.thebmw.minecraft.mods.thebmwtech.helpers;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by thebmw on 10/6/16.
 */
public class ItemRegistry {
    protected List<IRegistryEntry> entryList;

    public ItemRegistry() {
        entryList = new LinkedList<IRegistryEntry>();
    }

    public void AddEntry(IRegistryEntry entry) {
        entryList.add(entry);
    }

    public void registerItems() {
        //Purgatory.log.debug("Registering Purgatory Items");
        for (IRegistryEntry entry:
                entryList) {
            entry.registerItem();
        }
    }

    public void registerRecipies() {
        //Purgatory.log.debug("Registering Purgatory Recipies");
        for (IRegistryEntry entry:
                entryList) {
            entry.registerRecipes();
        }
    }


    public void registerRenderers() {
        //Purgatory.log.debug("Not Registering Renderers This Is A Server");
    }
}
