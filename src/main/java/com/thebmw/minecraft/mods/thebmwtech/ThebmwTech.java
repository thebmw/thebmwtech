package com.thebmw.minecraft.mods.thebmwtech;

import com.thebmw.minecraft.mods.thebmwtech.helpers.ItemRegistry;
import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;


@Mod(modid = ThebmwTech.MODID, version = ThebmwTech.VERSION)
public class ThebmwTech
{
    public static final String MODID = "thebmwtech";
    public static final String VERSION = "0.1-dev";

    @SidedProxy(clientSide = "com.thebmw.minecraft.mods.thebmwtech.helpers.ClientItemRegistry", serverSide = "com.thebmw.minecraft.mods.thebmwtech.helpers.ItemRegistry")
    public static ItemRegistry itemRegistry;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        ThebmwTechItems.init(itemRegistry);
        itemRegistry.registerItems();
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        itemRegistry.registerRecipies();
        itemRegistry.registerRenderers();
        // some example code

    }
}
