package com.thebmw.minecraft.mods.thebmwtech.helpers;

/**
 * Created by thebmw on 10/6/16.
 */
public class ClientItemRegistry extends ItemRegistry {
    public void registerRenderers() {

        for (IRegistryEntry entry:
                entryList) {
            entry.registerRenderer();
        }
    }
}
