package com.thebmw.minecraft.mods.thebmwtech;

import com.thebmw.minecraft.mods.thebmwtech.helpers.ItemRegistry;
import com.thebmw.minecraft.mods.thebmwtech.items.TestItem;
import net.minecraft.item.Item;

/**
 * Created by thebmw on 10/6/16.
 */
public class ThebmwTechItems {

    public static Item TEST_ITEM;

    public static void init(ItemRegistry registry) {
        TEST_ITEM = new TestItem(registry);
    }
}
