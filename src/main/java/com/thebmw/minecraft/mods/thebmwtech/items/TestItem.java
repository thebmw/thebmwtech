package com.thebmw.minecraft.mods.thebmwtech.items;

import com.thebmw.minecraft.mods.thebmwtech.ThebmwTechTab;
import com.thebmw.minecraft.mods.thebmwtech.helpers.ItemRegistry;
import com.thebmw.minecraft.mods.thebmwtech.helpers.bases.ItemBase;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by thebmw on 10/6/16.
 */
public class TestItem extends ItemBase {

    public TestItem(ItemRegistry registry) {
        super(registry);
        maxStackSize = 64;
        setCreativeTab(ThebmwTechTab.tabThebmwTech);
        setUnlocalizedName("test_item");

        setRegistryName("test_item");
    }

    @Override
    public void registerItem() {
        GameRegistry.register(this);
    }

    @Override
    public void registerRecipes() {

    }

    @Override
    public void registerRenderer() {

    }
}
