package com.thebmw.minecraft.mods.thebmwtech.helpers;

/**
 * Created by thebmw on 10/6/16.
 */
public interface IRegistryEntry {
    public void registerItem();
    public void registerRecipes();
    public void registerRenderer();
}
