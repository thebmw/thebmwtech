package com.thebmw.minecraft.mods.thebmwtech;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;

import static com.thebmw.minecraft.mods.thebmwtech.ThebmwTech.MODID;

/**
 * Created by thebmw on 10/6/16.
 */
public class ThebmwTechTab extends CreativeTabs {

    public static final @Nonnull CreativeTabs tabNoTab, tabThebmwTech;

    static {
        tabNoTab = new ThebmwTechTab();
        tabThebmwTech = new ThebmwTechTab(CreativeTabs.CREATIVE_TAB_ARRAY.length - 1);
    }

    public ThebmwTechTab() {
        super(MODID);
    }

    public ThebmwTechTab(int index) {
        super(index, MODID);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public @Nonnull String getTabLabel() {
        return MODID;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public @Nonnull
    String getTranslatedTabLabel() {
        return MODID;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public @Nonnull
    Item getTabIconItem() {
        return ThebmwTechItems.TEST_ITEM;
    }
}
