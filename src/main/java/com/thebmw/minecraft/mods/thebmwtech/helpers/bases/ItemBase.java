package com.thebmw.minecraft.mods.thebmwtech.helpers.bases;

import com.thebmw.minecraft.mods.thebmwtech.helpers.IRegistryEntry;
import com.thebmw.minecraft.mods.thebmwtech.helpers.ItemRegistry;
import net.minecraft.item.Item;

/**
 * Created by thebmw on 10/6/16.
 */
public abstract class ItemBase extends Item implements IRegistryEntry {
    public ItemBase(ItemRegistry registry) {
        super();
        registry.AddEntry(this);
    }
}
